<?php

class Chat 
{
    protected $headers = [];

    /**
     * Main functionality of the Chat-class
     * 
     * Methods:
     *      sendHeaders - forming and writing `html-headers` to socket
     *      newConnectionACK - handshake (first connection)
     *      newDisconnectionACK - opposite method from handshake (closing connection)
     *      sendDataToClient - sender method
     *      translateToBits - encoding message and headers into Frame, which use for transfer data in socket protocol
     *      decodeFromBites - opposite method of translate to bits
     *      createFinallyMessage - create a string with HTML tags for transfering it to front
     * 
     */
    public function sendHeaders ($headersAsString, $serverSocket, $host, $port): void {
        $tmpHeadersLines = explode (PHP_EOL, $headersAsString);
        
        foreach ($tmpHeadersLines as $key => $headerLine) {
            if (preg_match ('/\A(\S+): (.*)\z/', trim ($headerLine), $matches))
                $this->headers[trim ($matches[1])] = trim ($matches[2]);
        }

        $socketKey = $this->headers['Sec-WebSocket-Key'];
        $socketToken = base64_encode (pack ('H*', sha1 ($socketKey.'258EAFA5-E914-47DA-95CA-C5AB0DC85B11')));
    
        $responseHeaders = 
            "HTTP/1.1 101 Switching Protocols \r\n" .
            "Upgrade: websocket \r\n" .
            "Connection: Upgrade \r\n" .
            "WebSocket-Origin: $host \r\n" .
            "WebSocket-Location: wss://$host:$port/server.php \r\n" .
            "Sec-WebSocket-Accept: $socketToken \r\n\r\n"
        ;

        socket_write ($serverSocket, $responseHeaders, strlen ($responseHeaders));
    }

    /**
     * Handshake method
     */
    public function newConnectionACK ($clientIP): string {
        $message = "<hr><h6 color='#ccc'>New client ". $clientIP." connected at ".date("Y-m-d H:i:s")."</h6>";
        $messageArray = [
            "message" => $message,
            "type" => "newConnection"
        ];
        $preparedData = $this->translateToBits (json_encode ($messageArray));

        return $preparedData;
    }

    /**
     * Opposite of handshake
     */
    public function newDisconnectionACK ($clientIP): string {
        $message = "<hr><h6 style='color: #ff6666'>Client ". $clientIP." disconnected at ".date("Y-m-d H:i:s")."</h6>";
        $messageArray = [
            "message" => $message,
            "type" => "Disconnection"
        ];

        $preparedData = $this->translateToBits (json_encode ($messageArray));

        return $preparedData;
    }

    /**
     * Writing to concrete client socket, in fact, sending data to front
     */
    public function sendDataToClient ($message, $clientSocketsArray): bool {
        $messageLength = strlen ($message);

        if ($messageLength > 0) {
            foreach ($clientSocketsArray as $clientSocket) {
                @socket_write ($clientSocket, $message, $messageLength);
            }
    
            return true;
        } else 
            throw new \LogicException ("Message length lower or equal zero.");
    }

    /**
     * Encoding the header as bits string witch covered by mask (automatically).
     */
    public function translateToBits ($socketData): string {
        $b1 = 0x81;
        $length = strlen ($socketData);
        $header = "";

        if ($length <= 125) {
            $header = pack ('CC', $b1, $length);
        } else if ($length > 125 && $length < 65536) {
            $header = pack ('CCn', $b1, 126, $length);
        } else if ($length > 65536) {
            $header = pack ('CCNN', $b1, 127, $length);
        } else {
            throw new \LogicException (
                "$length is fit to any size"
            );
        }
        
        return $header.$socketData;
    }

    /**
     * Data, which passed as parametr has type string, has sequence of bits and had covered by mask.
     * Parameter has part data of full socket, because we read him in the loop. 
     * @param string $socketBufferedData
     */
    public function decodeFromBites (string $socketBufferedData): string {
        $length = ord ($socketBufferedData[1]) & 127;

        if ($length == 126) {
            $mask = substr ($socketBufferedData, 4, 4);
            $data = substr ($socketBufferedData, 8);
        } else if ($length == 127) {
            $mask = substr ($socketBufferedData, 10, 4);
            $data = substr ($socketBufferedData, 14);
        } else {
            $mask = substr ($socketBufferedData, 2, 4);
            $data = substr ($socketBufferedData, 6);
        }

        $socketResponseStr = "";
        for($i = 0; $i < strlen ($data); $i++) {
            $socketResponseStr .= $data[$i] ^ $mask[$i%4];
        }

        return $socketResponseStr;
    }

    /**
     * Forming finally string with HTML tags, translating to bits
     */
    public function createFinallyMessage (string $username, string $messageStr) {
        $message = "<hr><b>$username said:</b> $messageStr\n";

        $messageArray = [
            "message" => $message
        ];

        return $this->translateToBits (json_encode ($messageArray));
    }
}