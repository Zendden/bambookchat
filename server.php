<?php
require_once ("Classes/Chat.php");

/**
 * Server settings parameters
 */
$host = "bambookchat";
$port = 8899;

/**
 * Special variable for `socket_select` function
 */
$null = null;


$chat = new Chat ();

$socket = socket_create (AF_INET, SOCK_STREAM, SOL_TCP);
socket_set_option ($socket, SOL_SOCKET, SO_REUSEADDR, 1);
socket_bind ($socket, 0, $port);
socket_listen ($socket);

$clientSocketsArray = [$socket];

while (true) {
    $inputSocketsArray = $clientSocketsArray;
    socket_select ($inputSocketsArray, $null, $null, 0, 0);

    if (in_array ($socket, $inputSocketsArray)) {
        $connectedSocket = socket_accept ($socket);
        $clientSocketsArray[] = $connectedSocket;

        $input = socket_read ($connectedSocket, 1024);
        $chat->sendHeaders ($input, $connectedSocket, $host, $port);

        socket_getpeername ($connectedSocket, $clientIP);
        $connectionACK = $chat->newConnectionACK ($clientIP);
        $chat->sendDataToClient ($connectionACK, $clientSocketsArray);

        $clientSocketsArrayIndex = array_search ($socket, $inputSocketsArray);
        unset ($inputSocketsArray[$clientSocketsArrayIndex]);
    }

    foreach ($inputSocketsArray as $inputSocketResource) {

        while ((@socket_recv ($inputSocketResource, $socketBuffer, 1024, 0)) >= 1) {
            $socketMessage = $chat->decodeFromBites ($socketBuffer);
            $messageObject = json_decode ($socketMessage);

            if (!is_null ($messageObject->user)) {
                $finallyChatMessage = $chat->createFinallyMessage (
                    $messageObject->user, $messageObject->text
                );

                $chat->sendDataToClient ($finallyChatMessage, $clientSocketsArray);

                break 2;
            }
        }

        $socketData = @socket_read ($inputSocketResource, 1024, PHP_BINARY_READ);
        if ($socketData === FALSE) {
            socket_getpeername ($inputSocketResource, $clientIP);
            $disconnectedACK = $chat->newDisconnectionACK ($clientIP);
            $chat->sendDataToClient ($disconnectedACK, $clientSocketsArray);

            $clientSocketsArrayIndex = array_search ($inputSocketResource, $clientSocketsArray);
            unset ($clientSocketsArray[$clientSocketsArrayIndex]);
        }
    }

}

socket_close ();