<!DOCTYPE html>
<html lang="ru">

    <head>
        <meta charset="UTF-8">
        <title>BambookChat</title>

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" crossorigin="anonymous">
        <link rel="stylesheet" href="Styles/style.css">
    </head>

    <body id="main_body">
       
        <div class="container-fluid">
            <div id="form_control">
                <div class="row">
                    <div class="col">
                        <div id="output"></div>
                    </div>
                </div>
                <form id="chat-form">
                    <div class="row">
                        <div class="col" id="col_form">
                                <div class="form-group">
                                    <input type="text" class="form-control" id="nickname_field" placeholder="John Doe">
                                </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-10">
                            <div class="form-group">
                                <input type="textarea" class="form-control" id="message_field" placeholder="Message">
                            </div>
                        </div>
                        <div class="form-group col-md-2">
                            <div class="form-group">
                                <input type="submit" class="btn btn-success" id="button_field" value="Send">
                            </div>
                        </div>
                    </div>
                </form>
            </div>

        </div>
        
        <script src="https://code.jquery.com/jquery-3.4.1.js" crossorigin="anonymous"></script>
        <script src="scripts/script.js"></script>
    </body>

</html>
<!-- <form action="" id="chat-form" name="formbambook">
    <input type="text" name="chat-user" id="chat-user" placeholder="please, name">
    <input type="textarea" name="chat-text" id="chat-text" placeholder="type your message">
    <button>Send</button>
</form>
<div id="sysResult"></div> -->