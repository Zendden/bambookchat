function throwMessage(text) {
    $("#output").append(text);
}

$(document).ready(function($) {

    let socket = new WebSocket("ws://bambookchat:8899/server.php");

    socket.onopen = function() {
        throwMessage("<hr><h6>Connection successfully established</h6>");
    }

    socket.onerror = function(error) {
        throwMessage("<hr><h6 style='color: #ff6666'>Connection error " + error.message + "</h6>");
    };

    socket.onclose = function() {
        throwMessage("<hr><h6 style='color: #ff6666'>Connection closed</h6>");
    }

    socket.onmessage = function(eventObj) {
        var data = JSON.parse(eventObj.data);
        throwMessage(data.message);
        output = document.getElementById('output');
        output.scrollTop = Number(output.scrollHeight) + 57;
    }

    $("#chat-form").submit(function() {
        var tmpStr = JSON.stringify({
            user: $("#nickname_field").val(),
            text: $("#message_field").val()
        });

        socket.send(tmpStr);

        $("#message_field").val('');
        $("#nickname_field").attr('hidden', true);
        $("#output").css("height", document.documentElement.clientHeight - $("#chat-form").height());

        return false;
    });

    $("#main_body").css({ "width": document.documentElement.clientWidth, "height": document.documentElement.clientHeight });
    $("#output").css("height", document.documentElement.clientHeight - $("#chat-form").height());
});